package br.unicamp.ic.inf319;

import java.util.ArrayList;
import java.util.List;

public class BillOfMaterials {
	private List<AbstractPart> parts;
	
	public BillOfMaterials() {
		parts = new ArrayList<AbstractPart>();
	}

	public List<AbstractPart> getParts() {
		return parts;
	}

	public void addPart(AbstractPart p) {
		parts.add(p);
	}
	
	public void removePart(AbstractPart p) {
		parts.remove(p);
	}
	
	public void assemble() {
		
	}
}
