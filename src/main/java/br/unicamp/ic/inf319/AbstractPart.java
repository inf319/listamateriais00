package br.unicamp.ic.inf319;

public abstract class AbstractPart {
	private Integer id;
	private String name;
	
	public AbstractPart() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
